package ru.kharlamova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.api.IRepository;
import ru.kharlamova.tm.model.User;

import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    Optional<User> findByLogin(@NotNull String login);

    @Nullable
    Optional<User> findByEmail(@NotNull String email);

    @Nullable
    User removeByLogin(@NotNull String login);

}