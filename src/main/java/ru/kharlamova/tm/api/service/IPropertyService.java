package ru.kharlamova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.kharlamova.tm.api.other.ISaltSetting;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    String getDeveloperName();

}
